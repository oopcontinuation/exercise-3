#ifndef STACK_H
#define STACK_H

template< typename T, int s = 10 >
class Stack {
public:
	Stack(); // default constructor (stack size 10)
					 // destructor
	~Stack() {
		delete[] stackPtr;
	} // end ~Stack destructor
	bool push(const T&); // push an element onto the stack
	bool pop(T&); // pop an element off the stack
				  // determine whether Stack is empty
	bool isEmpty() const {
		return top == -1;
	} // end function isEmpty

	  // determine whether Stack is full
	bool isFull() const {
		return top == size - 1;
	} // end function isFull

private:
	int size; // # of elements in the stack
	int top; // location of the top element
	T *stackPtr; // pointer to the stack
}; 

   // constructor
template< typename T, int s = 10 >
Stack< T, s >::Stack() {
	size = s > 0 ? s : 10;
	top = -1; // Stack initially empty
	stackPtr = new T[size]; // allocate memory for elements
}
  // end Stack constructor
  
  // push element onto stack;
  // if successful, return true; otherwise, return false
template< typename T, int s >
bool Stack< T, s >::push(const T &pushValue) {
	if (!isFull()) {
		stackPtr[++top] = pushValue; // place item on Stack
		return true; // push successful
	} // end if
	return false; // push unsuccessful
} // end function push


  // pop element off stack;
  // if successful, return true; otherwise, return false
template< typename T, int s >
bool Stack< T, s >::pop(T &popValue) {
	if (!isEmpty()) {
		popValue = stackPtr[top--]; // remove item from Stack
		return true; // pop successful
	} // end if
	return false; // pop unsuccessful
} // end function pop
#endif