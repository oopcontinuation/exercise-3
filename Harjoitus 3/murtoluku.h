#ifndef MURTOLUKU_H
#define MURTOLUKU_H
#include <iostream>
class murtoluku {
public:
	murtoluku();
	murtoluku(int, int);
	~murtoluku();
	murtoluku & operator+=(const murtoluku i);
	friend std::ostream & operator<<(std::ostream& os, const murtoluku& obj);
private:
	int o;
	int n;
};
#endif