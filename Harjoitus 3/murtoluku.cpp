#include "murtoluku.h"

murtoluku::murtoluku() { murtoluku(1, 2); };
murtoluku::murtoluku(int osoitin, int nimettaja) {
	o = osoitin;
	n = nimettaja;
}
murtoluku::~murtoluku() {

}

murtoluku & murtoluku::operator+= (const murtoluku i) {
	this->o += i.o;
	return *this;
}

std::ostream & operator<<(std::ostream & os, const murtoluku & ml){
	os << ml.o << "/" << ml.n;
	return os;
}
