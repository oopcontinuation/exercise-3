// Stack class template test program. Function main uses a
// function template to manipulate objects of type Stack< T >.
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
#include "Stack.h" // Stack class template definition
#include "murtoluku.h"
// function template to manipulate Stack< T >
template< typename T, int s >
void testStack(
	Stack< T, s > &theStack, // reference to Stack< T >
	T value, // initial value to push
	T increment, // increment for subsequent values
	const char *stackName // name of the Stack < T > object
) {
	cout << "\nPushing elements onto " << stackName << '\n';
	while (theStack.push(value)) {
		cout << value << ' ';
		value += increment;
	}
	cout << "\nStack is full. Cannot push " << value << "\n\nPopping elements from " << stackName << '\n';
	while (theStack.pop(value)) cout << value << ' ';
	cout << "\nStack is empty. Cannot pop\n";
}

int main() {
	Stack< double, 5 > doubleStack;
	Stack< int > intStack;
	Stack<murtoluku, 5> mStack;
	testStack(doubleStack, 1.1, 1.1, "doubleStack");
	testStack(intStack, 1, 1, "intStack");
	testStack(mStack, murtoluku(1, 5), murtoluku(1, 5), "mStack");

	system("pause");
	return 0;
}